DOCKER          = docker
DOCKER_COMPOSE  = docker-compose

EXEC_PHP        = $(DOCKER_COMPOSE) exec -T php /entrypoint
COMPOSER        = $(EXEC_PHP) composer

##
##Project
##-------

install: ## Install and start the project
install: build start vendor

restart: ## Restart the project
restart: stop start

clean: ## Stop the project and remove generated files
clean: kill
	rm -rf ./app/vendor

reset: ## Stop and start a fresh install of the project
reset: clean install

build:
	@$(DOCKER_COMPOSE) pull --parallel --quiet --ignore-pull-failures 2> /dev/null
	$(DOCKER_COMPOSE) build --pull

start:
	$(DOCKER_COMPOSE) up -d --remove-orphans --no-recreate

stop:
	$(DOCKER_COMPOSE) stop

kill:
	$(DOCKER_COMPOSE) kill
	$(DOCKER_COMPOSE) down --volumes --remove-orphans

#COMPOSER
composer.lock: ./app/composer.json
	$(COMPOSER) update --lock --no-scripts --no-interaction

vendor: ./app/composer.lock
	$(COMPOSER) install

##
##Tests
##-------

tests: ## Run tests
	$(EXEC_PHP) vendor/bin/phpunit .

##
##Documentation
##-----

.DEFAULT_GOAL := doc
doc: ## List Makefile commands
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
