<?php

declare(strict_types=1);

namespace Vanilla;

class Hello
{
    public static function sayHello(string $name): string
    {
        return sprintf('Hello %s', $name);
    }
}
