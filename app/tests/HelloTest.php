<?php

declare(strict_types=1);

namespace Vanilla\Tests;

use Vanilla\Hello;
use PHPUnit\Framework\TestCase;

class HelloTest extends TestCase
{
    public function testSayHelloWithNameShouldReturnHelloAndName(): void
    {
        // GIVEN
        $name = 'John';
        // WHEN
        $result = Hello::sayHello($name);
        // THEN
        self::assertEquals(expected: 'Hello John', actual: $result);
    }
}
